<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class ClientController extends Controller
{
    public function list()
    {
        $Users = User::paginate(15);
        
        return view('index' , ['Users' => $Users]);
    }

    public function search(Request $req)
    {
        return $req->input();
    }
}
