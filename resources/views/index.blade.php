@extends('Layout')

@section('content')

<h1>Liste des clients</h1>

<ul class="list-group">
    @foreach ($Users as $User)
        <li class="list-group-item">{{$User->firstname}} - {{$User->lastname}} - {{$User->email}} - {{$User->location}} </li>
    @endforeach
</ul>

<div class="row d-flex justify-content-center">
    {{$Users->links()}}
</div>
    
@endsection